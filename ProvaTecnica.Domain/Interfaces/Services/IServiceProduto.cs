﻿using ProvaTecnica.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Domain.Interfaces.Services
{
   public interface IServiceProduto
    {
        IList<Produto> Listar();
        //bool ExisteProdutoNaCategoria(Guid idCategoria);
        Produto Adicionar(Produto produto);
        Produto ObterPorNome(string nome);
        Produto ObterPorId(Guid id);
        Produto Atualizar(Produto produto);
        bool RemoverPorId(string id);
    }
}
