﻿using ProvaTecnica.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Domain.Interfaces.Services
{
    public interface IServiceCategoria
    {
        Categoria ObterPorNome(string categoria);
        IList<Categoria> Listar();
        Categoria Adicionar(Categoria categoria);
        Categoria ObterPorId(Guid id);
        Categoria Atualizar(Categoria categoria);
        bool RemoverPorId(string id);
    }
}
