﻿using ProvaTecnica.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Domain.Interfaces.Repositories
{
    public interface IRepositoryCategoria
    {
        Categoria ObterPorNome(string categoria);
        IList<Categoria> Listar();
        Categoria ObterPorId(Guid id);        
        Categoria Adicionar(Categoria categoria);
        Categoria Atualizar(Categoria categoria);
        bool RemoverPorId(Guid id);

    }
}
