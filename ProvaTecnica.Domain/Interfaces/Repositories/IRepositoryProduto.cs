﻿using ProvaTecnica.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Domain.Interfaces.Repositories
{
    public interface IRepositoryProduto
    {
        IList<Produto> Listar();
        bool ExisteProdutoNaCategoria(Guid idCategoria);
        Produto ObertPorId(Guid id);
        Produto Adicionar(Produto produto);
        Produto Atualizar(Produto produto);
        bool RemoverPorId(Guid id);
    }
}
