﻿using ProvaTecnica.Domain.Entities;
using ProvaTecnica.Domain.Interfaces.Repositories;
using ProvaTecnica.Domain.Interfaces.Services;
using ProvaTecnica.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Domain.Services
{
    public class ServiceProduto : IServiceProduto
    {
        IRepositoryProduto _repositoryproduto;
        IRepositoryCategoria _repositoriocategoria;

        public ServiceProduto(IRepositoryProduto _repositoryproduto, IRepositoryCategoria _repositoriocategoria)
        {
            this._repositoryproduto = _repositoryproduto;
            this._repositoriocategoria = _repositoriocategoria;
        }

        public Produto Adicionar(Produto produto)
        {

            if (_repositoriocategoria.ObterPorId(produto.IdCategoria) == null)
                throw new Exception("Categoria inválida");

            if (string.IsNullOrEmpty(produto.Nome))
                throw new Exception("Nome do produto precisa ser preenchido");  
          
            return _repositoryproduto.Adicionar(produto);       

        }

        public Produto Atualizar(Produto produto)
        {

            if (_repositoriocategoria.ObterPorId(produto.Id) != null)
                return _repositoryproduto.Atualizar(produto);
            throw new NotImplementedException();
        }
        
        public IList<Produto> Listar()
        {
            return _repositoryproduto.Listar();
        }

        public Produto ObterPorId(Guid id)
        {
            return _repositoryproduto.ObertPorId(id);
        }
        

        public Produto ObterPorNome(string nome)        {

            // return _repositoryproduto.
            return null;
        }

        public bool RemoverPorId(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new Exception("Id inválido");

            Guid _id = HandleGuid.Init(id);
            return _repositoryproduto.RemoverPorId(_id);
        }
    }
}
