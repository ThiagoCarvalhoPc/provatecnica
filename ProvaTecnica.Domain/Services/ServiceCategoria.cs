﻿using ProvaTecnica.Domain.Entities;
using ProvaTecnica.Domain.Interfaces.Repositories;
using ProvaTecnica.Domain.Interfaces.Services;
using ProvaTecnica.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Domain.Services
{
    public class ServiceCategoria : IServiceCategoria
    {
        private readonly IRepositoryCategoria _repositoryCategoria;
        private readonly IRepositoryProduto _repositoryproduto;

        public ServiceCategoria(IRepositoryCategoria _repositoryCategoria, IRepositoryProduto _repositoryproduto)
        {
            this._repositoryCategoria = _repositoryCategoria;
            this._repositoryproduto = _repositoryproduto;
        }

        public Categoria Adicionar(Categoria categoria)
        {
            this.IsSafe(categoria);
            return _repositoryCategoria.Adicionar(categoria);
        }

        public Categoria Atualizar(Categoria categoria)
        {
            this.IsSafe(categoria);
            return _repositoryCategoria.Atualizar(categoria);
        }

        public IList<Categoria> Listar()
        {
            return _repositoryCategoria.Listar();
        }

        public Categoria ObterPorId(Guid id)
        {
            if (id == null)
                throw new Exception();

            return _repositoryCategoria.ObterPorId(id);
        }
        public Categoria ObterPorNome(string categoria)
        {
            if (string.IsNullOrEmpty(categoria))
                throw new Exception();

            return _repositoryCategoria.ObterPorNome(categoria);
        }

        public bool RemoverPorId(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new Exception("Id inválido");

            Guid _id = HandleGuid.Init(id);

            if (_repositoryproduto.ExisteProdutoNaCategoria(_id))
                throw new Exception("Existem produtos relacionados a essa categoria");

           
            return _repositoryCategoria.RemoverPorId(_id);
        }

        private void IsSafe(Categoria categoria)
        {
            //Necessário que a categoria tenha um nome
            if (categoria.Nome == null)
                throw new Exception("Nome inválido");

            //Não é possível adicionar uma categoria com nome já existente
            if (this.ObterPorNome(categoria.Nome) != null)
                throw new Exception("Categoria já existe");
        }
    }
}
