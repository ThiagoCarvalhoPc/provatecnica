﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Domain.Entities
{
    public class Produto
    {
        public Guid Id { get; set; }
        public Guid IdCategoria { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
      

    }
}
