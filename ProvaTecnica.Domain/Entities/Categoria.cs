﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Domain.Entities
{
   public class Categoria
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }       
        public string Descricao { get; set; }
       
       
    }
}
