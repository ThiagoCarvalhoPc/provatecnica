﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Domain.Shared
{
    public class HandleGuid
    {
        public static Guid Init(string id)
        {
            Guid _id = new Guid(id);
            if (_id == null)
                throw new Exception();
            return _id;

        }
    }
}
