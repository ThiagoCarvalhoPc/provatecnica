﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using ProvaTecnica.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Infra.Persistence.Map
{

    public class InstallmentMap
    {


        public void Map()
        {
            //Trata o GUID para a formatação do MongoDb
            BsonDefaults.GuidRepresentation = GuidRepresentation.PythonLegacy;

            BsonClassMap.RegisterClassMap<Categoria>(map =>
                {   

                    map.AutoMap();
                    map.MapIdMember(x => x.Id).SetIdGenerator(CombGuidGenerator.Instance);
                    map.SetIgnoreExtraElements(true);                    

                });
        }
    }
}

