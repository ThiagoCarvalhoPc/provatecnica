﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using ProvaTecnica.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Infra.Persistence.Map
{
    public class InstallMap
    {
        public InstallMap()
        {
            BsonDefaults.GuidRepresentation = GuidRepresentation.PythonLegacy;
        }
        public void Map()
        {

            BsonClassMap.RegisterClassMap<Categoria>(map =>
            {                

                map.AutoMap();
                map.MapIdMember(x => x.Id).SetIdGenerator(CombGuidGenerator.Instance);
                map.SetIgnoreExtraElements(true);
                

            });
        }
    }
}
