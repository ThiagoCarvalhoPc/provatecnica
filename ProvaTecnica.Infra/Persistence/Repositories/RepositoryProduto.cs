﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using ProvaTecnica.Domain.Entities;
using ProvaTecnica.Domain.Interfaces.Repositories;
using ProvaTecnica.Infra.Persistence.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Infra.Persistence.Repositories
{
    public class RepositoryProduto :RepositoryBase, IRepositoryProduto
    {
        private readonly IMongoCollection<Produto> _produto;
        public RepositoryProduto(IConfiguration config)
            :base(config)
        {
            //var client = new MongoClient(config.GetSection("ConnectionUrlPort").Value);
            //var database = client.GetDatabase("local");
            _produto = _mongodatabase.GetCollection<Produto>("produto");
        }

        public Produto Adicionar(Produto produto)
        {
            _produto.InsertOne(produto);
            return produto;          
        }

        public Produto Atualizar(Produto produto)
        {
            _produto.ReplaceOne(x => x.Id == produto.Id, produto);
            return produto;
            throw new NotImplementedException();
        }

        public IList<Produto> Listar()
        {
            return _produto.Find(x => true)
                .ToList();           
        }

        public Produto ObertPorId(Guid id)
        {

         return   _produto.Find(x => x.Id == id)
                .FirstOrDefault();
        }
       
        public bool ExisteProdutoNaCategoria(Guid idCategoria)
        {
           return _produto.Find(x => x.IdCategoria == idCategoria)
                .Any();
            
        }
        public bool RemoverPorId(Guid id)
        {
            try
            {
                _produto.DeleteOne(x => x.Id == id);
                return true;
            }
            catch { return false; }

        }
    }
}
