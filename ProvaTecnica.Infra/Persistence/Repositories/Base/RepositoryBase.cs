﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Infra.Persistence.Repositories.Base
{
    public abstract class RepositoryBase
    {
        protected IMongoDatabase _mongodatabase;
        public RepositoryBase(IConfiguration config )
        {
            try
            {
                var client = new MongoClient(config.GetSection("ConnectionUrlPort").Value);
                _mongodatabase = client.GetDatabase(config.GetSection("DatabaseName").Value);
            }
            catch { throw new Exception("Erro ao se conectar ao Banco"); }
        }
    }
}
