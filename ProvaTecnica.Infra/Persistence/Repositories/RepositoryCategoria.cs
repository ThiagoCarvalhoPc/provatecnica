﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using ProvaTecnica.Domain.Entities;
using ProvaTecnica.Domain.Interfaces.Repositories;
using ProvaTecnica.Infra.Persistence.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProvaTecnica.Infra.Persistence.Repositories
{
    public class RepositoryCategoria :RepositoryBase, IRepositoryCategoria
    {
        private readonly IMongoCollection<Categoria> _categoria;

        public RepositoryCategoria(IConfiguration config)
            :base(config)
        {           
            
            _categoria = _mongodatabase.GetCollection<Categoria>("categoria");
        }


        public Categoria Adicionar(Categoria categoria)
        {
            _categoria.InsertOne(categoria);
            return categoria;
        }

        public Categoria Atualizar(Categoria categoria)
        {
            _categoria.ReplaceOne(x => x.Id == categoria.Id, categoria);
            return categoria;
        }

        public IList<Categoria> Listar()
        {
            return _categoria.Find(x => true)
                   .ToList();
        }

        public Categoria ObterPorId(Guid id)
        {
            return _categoria.Find(categoria => categoria.Nome != null)
                       .FirstOrDefault();
        }

        public Categoria ObterPorNome(string categoria)
        {
           return _categoria.Find(x => x.Nome.Equals(categoria))
                .FirstOrDefault();
        }

        public bool RemoverPorId(Guid id)
        {
            try
            {
                _categoria.DeleteOne(x => x.Id == id);
                return true;
            }
            catch { return false; }
        }
    }
}
