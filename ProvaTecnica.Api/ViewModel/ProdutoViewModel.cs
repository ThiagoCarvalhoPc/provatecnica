﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProvaTecnica.Api.ViewModel
{
    public class ProdutoViewModel
    {
        public string Id { get; set; }
        public string IdCategoria { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        //public decimal PrecoCompra { get; set; }
        //public decimal PrecoVenda { get; set; }
        //public bool Ativo { get; set; }
        //public DateTime DataCadastro { get; set; }
        //public int Quantidade { get; set; }

    }
}
