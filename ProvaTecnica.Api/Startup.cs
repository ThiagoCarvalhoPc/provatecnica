﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProvaTecnica.Api.AutoMapper;
using ProvaTecnica.Domain.Interfaces.Repositories;
using ProvaTecnica.Domain.Interfaces.Services;
using ProvaTecnica.Domain.Services;
using ProvaTecnica.Infra.Persistence.Map;
using ProvaTecnica.Infra.Persistence.Repositories;


namespace ProvaTecnica.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            new InstallmentMap().Map();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            IMapper mapper = AutoMapperConfig.RegisterMappings().CreateMapper();
            services.AddSingleton(mapper);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);           


            services.AddScoped<IServiceCategoria, ServiceCategoria>();
                services.AddScoped<IRepositoryCategoria, RepositoryCategoria>();

                services.AddScoped<IServiceProduto, ServiceProduto>();
                services.AddScoped<IRepositoryProduto, RepositoryProduto>();

            
            }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            
           
        }
    }
}
