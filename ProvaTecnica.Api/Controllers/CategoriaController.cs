﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ProvaTecnica.Domain.Entities;
using ProvaTecnica.Domain.Interfaces.Repositories;
using ProvaTecnica.Domain.Interfaces.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
/// <summary>
/// Teste
/// </summary>
namespace ProvaTecnica.Api.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    public class CategoriaController : Controller
    {
        IServiceCategoria _servicecategoria;


        public CategoriaController(IServiceCategoria _servicecategoria)
        {
            this._servicecategoria = _servicecategoria;
        }



        [HttpGet]
        [Route("listar")]
        public IActionResult ListarCategorias()
        {
            try
            {
                return Ok(_servicecategoria.Listar());
            }
            catch (Exception x)
            {
                return NotFound(
                    new
                    {
                        Mensagem = x.Message,
                        Erro = true
                    });
            }
        }

        [HttpPost]
        [Route("inserir")]
        public IActionResult InserirCategoria([FromBody] Categoria categoria)
        {
            try
            {
                return Ok(_servicecategoria.Adicionar(categoria));
            }
            catch (Exception x)
            {
                return NotFound(
                    new
                    {
                        Mensagem = x.Message,
                        Erro = true
                    });
            }


        }

        [HttpDelete("{id}")]
        [Route("remover")]
        public IActionResult RemoverPorId([FromQuery] string id)
        {
            try
            {
                return Ok(_servicecategoria.RemoverPorId(id));
            }
            catch (Exception x)
            {
                return NotFound(
                    new
                    {
                        Mensagem = x.Message,
                        Erro = true
                    });
            }
        }

        [HttpPut]
        [Route("atualizar")]
        public IActionResult Atualizar([FromBody] Categoria categoria)
        {
            try
            {
              return Ok( _servicecategoria.Atualizar(categoria));
            }
            catch (Exception x)
            {
                return NotFound(
                    new
                    {
                        Mensagem = x.Message,
                        Erro = true
                    });
            }
        }

    }
}