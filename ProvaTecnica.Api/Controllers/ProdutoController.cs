﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ProvaTecnica.Api.ViewModel;
using ProvaTecnica.Domain.Entities;
using ProvaTecnica.Domain.Interfaces.Repositories;
using ProvaTecnica.Domain.Interfaces.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProvaTecnica.Api.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    public class ProdutoController : Controller
    {
        IServiceProduto _serviceproduto;       
        IMapper _mapper;
        
        public ProdutoController(IServiceProduto _serviceproduto, IMapper _mapper)
        {
            this._serviceproduto = _serviceproduto;
            //this._repositoryproduto = _repositoryproduto;
            this._mapper = _mapper;
        }


        [HttpGet]
        [Route("listar")]
        public IActionResult Listar()
        {
            try
            {
                return Ok(_serviceproduto.Listar());
            }
            catch (Exception x)
            {
                return NotFound(
                   new
                   {
                       Mensagem = x.Message,
                       Erro = true
                   });
            }

        }

        [HttpPost]
        [Route("inserir")]
        public IActionResult InserirProduto([FromBody] ProdutoViewModel produtoViewModel)
        {
            try
            {
                var produto = _mapper.Map<ProdutoViewModel, Produto>(produtoViewModel);
                return Ok(_serviceproduto.Adicionar(produto));

            }
            catch (Exception x)
            {
                return NotFound(
                   new
                   {
                       Mensagem = x.Message,
                       Erro = true
                   });
            }
        }

        [HttpPost]
        [Route("atualizar")]
        public IActionResult Atualizar([FromBody] ProdutoViewModel produtoViewModel)
        {
            var produto = _mapper.Map<ProdutoViewModel, Produto>(produtoViewModel);
            _serviceproduto.Atualizar(produto);
            return null;
        }

        [HttpGet("{id}")]
        [Route("remover")]
        public IActionResult Remover([FromQuery]string Id)
        {
            try
            {
                return Ok(_serviceproduto.RemoverPorId(Id));
            }
            catch (Exception x)
            {
                return NotFound(
                   new
                   {
                       Mensagem = x.Message,
                       Erro = true
                   });
            }

        }

    }
}
