﻿using AutoMapper;
using ProvaTecnica.Api.ViewModel;
using ProvaTecnica.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProvaTecnica.Api.AutoMapper
{
    public class ViewModelToDomain:Profile
    {
        public ViewModelToDomain()
        {
            CreateMap<ProdutoViewModel, Produto>();
        }
    }
}
